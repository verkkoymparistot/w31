import React from "react";
import "./Tori.css";

const Tori = () => {
  return (
    <div className="tori">
    <div className="txt">
      <input type="text" className="text" value="Hakusana ja/tai postinumero" />
      <select className="what">
        <option id="1">Kaikki osastot</option>
      </select>
      <select className="where">
        <option id="1">Koko Suomi</option>
      </select>
    </div>
      <input type="checkbox" name="how" />
      Myydään
      <input type="checkbox" className="how" />
      Ostetaan
      <input type="checkbox" className="how" />
      Vuokrataan
      <input type="checkbox" className="how" />
      Halutaan vuokrata
      <input type="checkbox" className="how" />
      Annetaan 
      <p className="hbtn">Tallenna haku</p>
      <button className="btn">Hae</button>
    </div>
  );
};

export default Tori;
